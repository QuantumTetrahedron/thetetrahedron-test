
#include "PauseSceneManager.h"

void PauseSceneManager::Start() {
    TTH::Input::EnableCursor();
}

void PauseSceneManager::Update(float dt) {
    if(TTH::Input::GetButton(TTH::Key::Escape)){
        TTH::SceneManager::ResumeSceneRequest();
    }
}

void PauseSceneManager::OnLeave() {
    TTH::Input::DisableCursor();
}