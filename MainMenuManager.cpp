
#include <TTH/Input/Input.h>
#include "MainMenuManager.h"

void MainMenuManager::Start() {
    TTH::Input::EnableCursor();
    audio = GetParent()->GetComponent<TTH::AudioSource>();
    volume = 1.0f;
}

void MainMenuManager::Update(float dt) {

    if(TTH::Input::GetButton(TTH::Key::E)){
        audio->TogglePause();
    }

    if(TTH::Input::GetButton(TTH::Key::Equal)){
        volume += 0.1;
        audio->SetVolume(volume);
    }

    if(TTH::Input::GetButton(TTH::Key::Minus)){
        volume -= 0.1;
        if(volume < 0) volume = 0;
        audio->SetVolume(volume);
    }
}

void MainMenuManager::OnLeave() {
}