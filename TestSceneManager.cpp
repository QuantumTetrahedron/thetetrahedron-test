
#include "TestSceneManager.h"
#include "TTH/Input/Input.h"
#include "TTH/SceneManagement/SceneManager.h"

void TestSceneManager::Start() {
    TTH::Input::DisableCursor();
}

void TestSceneManager::Update(float dt) {
    if(TTH::Input::GetButton(TTH::Key::Escape)){
        TTH::SceneManager::PauseAndLoadSceneRequest("Pause");
    }

}

void TestSceneManager::OnLeave() {

}
