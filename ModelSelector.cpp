
#include <TTH/Core/ObjectManager.h>
#include "ModelSelector.h"

void ModelSelector::Start() {
    obj = TTH::ObjectManager::GetObject(objName)->GetComponent<TTH::RenderComponent>();
    if(!models.empty()){
        obj->SetModel(models[0]);
    }
    currentModel = 0;
}

void ModelSelector::Update(float dt) {

}

void ModelSelector::OnLeave() {

}

bool ModelSelector::LoadFromFile(const TTH::IniFile &file) {
    std::string _models;
    file.RequireValue("models", _models);
    std::stringstream ss(_models);
    std::string model;
    while(ss >> model){
        models.push_back(model);
    }
    file.RequireValue("object", objName);
    return true;
}

void ModelSelector::ChangeModel(int direction) {
    if(models.empty()) return;
    currentModel += direction;

    while(currentModel < 0)
        currentModel = models.size() + currentModel;

    while(currentModel >= models.size())
        currentModel = currentModel - models.size();

    obj->SetModel(models[currentModel]);
}
