
#include <TTH/SceneManagement/SceneManager.h>
#include "QuitToMenuButton.h"

void QuitToMenuButton::Start() {
    sprite = GetParent()->GetComponent<TTH::SpriteComponent>();
}

void QuitToMenuButton::Update(float dt) {

}

void QuitToMenuButton::OnLeave() {

}

void QuitToMenuButton::OnMouseDown(int button) {
    if(button == 0)
        TTH::SceneManager::LoadNextSceneRequest("MainMenu");
}

void QuitToMenuButton::OnMouseEnter() {
    sprite->SetTexture("button_hovered");
}

void QuitToMenuButton::OnMouseLeave() {
    sprite->SetTexture("button");
}