
#ifndef THETETRAHEDRONTEST_BACKGROUNDCOMPONENT_H
#define THETETRAHEDRONTEST_BACKGROUNDCOMPONENT_H

#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"
#include "TTH/UI/SpriteComponent.h"

class BackgroundComponent : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override {}
    void OnLeave() override {}
private:
    std::shared_ptr<TTH::SpriteComponent> spriteComponent;

    TTH_COMPONENT(BackgroundComponent)
};


#endif //THETETRAHEDRONTEST_BACKGROUNDCOMPONENT_H
