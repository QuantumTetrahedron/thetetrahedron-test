
#include "TTH/Core/Game.h"
#include <TTH/Resources/ResourceManager.h>

int main()
{
    TTH::Initializer initializer;
    initializer.game.windowWidth = 800;
    initializer.game.windowHeight = 600;
    initializer.game.title = "The TetrahedronTest";
    initializer.game.startScene = "MainMenu";

    initializer.game.mouseEnabled = true;
    initializer.game.mouseButtonMode = "all";

    initializer.rendering.blending = true;
    initializer.rendering.depthTesting = true;


    TTH::Game::Initialize(initializer);

    TTH::Input::AddAxis("Horizontal", {TTH::Key::D, TTH::Key::Right}, {TTH::Key::A, TTH::Key::Left});
    TTH::Input::AddAxis("Vertical", {TTH::Key::W, TTH::Key::Up}, {TTH::Key::S, TTH::Key::Down});

    TTH::Input::AddAxis("Jump", {TTH::Key::Space}, {});

    TTH::ResourceManager::LoadFromFile("GameData/ResourceOptions.ini");

    TTH::Game::Start();

    TTH::Game::MainLoop();

    TTH::Game::Shutdown();

    return 0;
}