
#include "BackgroundComponent.h"
#include "TTH/Graphics/Gfx.h"

void BackgroundComponent::Start() {
    spriteComponent = GetParent()->GetComponent<TTH::SpriteComponent>();
    spriteComponent->SetTexture(TTH::Gfx::GetLastFrameTexture());
}