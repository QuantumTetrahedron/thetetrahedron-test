
#include "ResumeButtonController.h"
#include "TTH/SceneManagement/SceneManager.h"

void ResumeButtonController::Start() {
    sprite = GetParent()->GetComponent<TTH::SpriteComponent>();
}

void ResumeButtonController::Update(float dt) {

}

void ResumeButtonController::OnLeave() {

}

void ResumeButtonController::OnMouseDown(int button) {
    if(button == 0)
        TTH::SceneManager::ResumeSceneRequest();
}

void ResumeButtonController::OnMouseEnter() {
    sprite->SetTexture("button_hovered");
}

void ResumeButtonController::OnMouseLeave() {
    sprite->SetTexture("button");
}
