
#ifndef THETETRAHEDRONTEST_PAUSESCENEMANAGER_H
#define THETETRAHEDRONTEST_PAUSESCENEMANAGER_H

#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"
#include "TTH/Input/Input.h"
#include "TTH/SceneManagement/SceneManager.h"

class PauseSceneManager : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

    TTH_COMPONENT(PauseSceneManager)
};


#endif //THETETRAHEDRONTEST_PAUSESCENEMANAGER_H
