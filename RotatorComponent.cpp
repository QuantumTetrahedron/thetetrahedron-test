
#include "RotatorComponent.h"
#include "TTH/Core/ObjectManager.h"
#include "TTH/Utils/IniFile.h"

void RotatorComponent::Start() {
    toRotate = &TTH::ObjectManager::GetObject(rotatedObject)->transform;
}

void RotatorComponent::Update(float dt) {
    toRotate->rotation.y += dt*speed*30;
}

void RotatorComponent::OnLeave() {
}

bool RotatorComponent::LoadFromFile(const TTH::IniFile &file) {
    file.GetValue("speed", speed, 1.0f);
    file.RequireValue("rotatedObject", rotatedObject);
    return true;
}
