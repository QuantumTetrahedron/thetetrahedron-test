
#include "ClickableComponent.h"

void ClickableComponent::Start() {
    auto p = GetParent();
    lc = p->GetComponent<PointLight>();
    rc = p->GetComponent<TTH::RenderComponent>();
}

void ClickableComponent::Update(float dt) {

}

void ClickableComponent::OnLeave() {

}

void ClickableComponent::OnMouseDown(int button) {
    if(button == 0) {
        lc->color = glm::vec3(0.0f,1.0f,0.0f);
        rc->colorTint = glm::vec3(0.0f,1.0f,0.0f);
    }
}

void ClickableComponent::OnMouseEnter() {
    lc->color = glm::vec3(0.0f,0.0f,1.0f);
    rc->colorTint = glm::vec3(0.0f,0.0f,1.0f);
}

void ClickableComponent::OnMouseLeave() {
    lc->color = glm::vec3(1.0f,0.0f,0.0f);
    rc->colorTint = glm::vec3(1.0f,0.0f,0.0f);
}
