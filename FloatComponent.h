
#ifndef THETETRAHEDRON_FLOATCOMPONENT_H
#define THETETRAHEDRON_FLOATCOMPONENT_H


#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"

class FloatComponent : public TTH::BehaviourComponent {
public:
    FloatComponent();

    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

private:
    float floatRange;
    float floatSpeed;
    float currentOffset;

    bool goingUp;

    bool LoadFromFile(const TTH::IniFile& file) override;

    TTH_COMPONENT(FloatComponent)
};


#endif //THETETRAHEDRON_FLOATCOMPONENT_H
