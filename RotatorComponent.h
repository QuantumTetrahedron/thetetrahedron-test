
#ifndef THETETRAHEDRONTEST_ROTATORCOMPONENT_H
#define THETETRAHEDRONTEST_ROTATORCOMPONENT_H


#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"

class RotatorComponent : public TTH::BehaviourComponent{

public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

private:
    float speed;
    std::string rotatedObject;
    TTH::Transform* toRotate;

    bool LoadFromFile(const TTH::IniFile& file) override;

    TTH_COMPONENT(RotatorComponent)
};


#endif //THETETRAHEDRONTEST_ROTATORCOMPONENT_H
