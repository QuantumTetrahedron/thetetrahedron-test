
#ifndef THETETRAHEDRONTEST_NEWGAMEBUTTON_H
#define THETETRAHEDRONTEST_NEWGAMEBUTTON_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include <TTH/UI/SpriteComponent.h>

class NewGameButton : public TTH::BehaviourComponent{
public:

    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

    void OnMouseDown(int button) override;
    void OnMouseEnter() override;
    void OnMouseLeave() override;
private:
    std::shared_ptr<TTH::SpriteComponent> sprite;

    TTH_COMPONENT(NewGameButton)
};


#endif //THETETRAHEDRONTEST_NEWGAMEBUTTON_H
