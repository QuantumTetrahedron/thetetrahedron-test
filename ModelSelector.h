
#ifndef THETETRAHEDRONTEST_MODELSELECTOR_H
#define THETETRAHEDRONTEST_MODELSELECTOR_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Graphics/RenderComponent.h>
#include <TTH/Core/ObjectFactory.h>

class ModelSelector : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void ChangeModel(int direction);

protected:
    std::vector<std::string> models;

    int currentModel;

    std::string objName;
    std::shared_ptr<TTH::RenderComponent> obj;

    bool LoadFromFile(const TTH::IniFile &file) override;

    TTH_COMPONENT(ModelSelector)
};


#endif //THETETRAHEDRONTEST_MODELSELECTOR_H
