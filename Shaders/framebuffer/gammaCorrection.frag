#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;

void main()
{
    float gamma = 1.0;
    vec4 tex = texelFetch(screenTexture, ivec2(TexCoords),0);
    FragColor = vec4(pow(tex.rgb, vec3(1.0/gamma)), tex.a);
}