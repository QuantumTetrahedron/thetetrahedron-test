#version 330 core
out vec4 FragColor;

in vec2 TexCoords;

uniform sampler2D screenTexture;

vec2 offsets[9] = vec2[](
    vec2(-1,1), vec2(0,1), vec2(1,1),
    vec2(-1,0), vec2(0,0), vec2(1,0),
    vec2(-1,-1), vec2(0,-1), vec2(1,-1)
);

float kernel[9] = float[](
    1, 1, 1,
    1, -8, 1,
    1, 1, 1
);

void main()
{
    vec3 sampleTex[9];
    for(int i = 0; i < 9; i++)
    {
        sampleTex[i] = vec3(texelFetch(screenTexture, ivec2(TexCoords.st + offsets[i]), 0));
    }

    vec3 col = vec3(0.0);
    for(int i = 0; i < 9; i++)
        col += sampleTex[i] * kernel[i];
    col = abs(col);

    float c = max(col.x, max(col.y, col.z));
    vec3 originalCol = vec3(texelFetch(screenTexture, ivec2(TexCoords), 0));
    FragColor = vec4(originalCol - vec3(c), 1.0);
    //FragColor = vec4(c,c,c,1.0);
}