#version 330 core

in vec2 texCoords;
out vec4 color;

uniform sampler2D text;
uniform vec3 colorTint;

void main() {
	color = vec4(1.0,1.0,1.0,texture(text, texCoords).r) * vec4(colorTint, 1.0f);
}
