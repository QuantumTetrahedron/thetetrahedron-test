#version 330 core

out vec4 fragColor;

in vec3 FragPos;
in vec3 Normal;
in vec2 TexCoords;

uniform vec3 colorTint;
uniform vec3 viewPos;

struct Light{
    vec3 position;
    vec3 color;
};

uniform Light pointLight;
uniform Light pointLight2;

uniform sampler2D texture_diffuse1;

vec3 CalcLight(Light light){
    float ambient = 0.01;

    vec3 lightDir = normalize(light.position - FragPos);
    float diffuse = max(dot(lightDir, Normal), 0.0);

    vec3 viewDir = normalize(viewPos - FragPos);
    vec3 halfwayDir = normalize(lightDir + viewDir);
    float specular = 0.5 * pow(max(dot(Normal, halfwayDir), 0.0), 32.0) * max(sign(diffuse), 0.0);

    vec3 outColor = (ambient + diffuse + specular) * light.color;
    return outColor;
}

void main() {
    vec3 material_diffuse = texture( texture_diffuse1, TexCoords ).rgb * colorTint;

    vec3 outColor = CalcLight(pointLight) + CalcLight(pointLight2);
    outColor = outColor * material_diffuse;

    fragColor = vec4(outColor, 1.0);
}
