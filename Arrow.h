
#ifndef THETETRAHEDRONTEST_ARROW_H
#define THETETRAHEDRONTEST_ARROW_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Core/ComponentFactory.h>
#include "ModelSelector.h"

class Arrow : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void OnMouseDown(int button) override;

    void OnMouseEnter() override;

    void OnMouseLeave() override;

protected:
    bool LoadFromFile(const TTH::IniFile &file) override;

    int direction;

    std::string objName;
    std::shared_ptr<ModelSelector> modelSelector;
    std::shared_ptr<TTH::RenderComponent> rc;

    TTH_COMPONENT(Arrow)
};


#endif //THETETRAHEDRONTEST_ARROW_H
