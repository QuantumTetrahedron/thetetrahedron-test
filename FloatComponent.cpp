
#include "FloatComponent.h"
#include "TTH/Core/Object.h"
#include "TTH/Utils/IniFile.h"

void FloatComponent::Start() {
    currentOffset = 0.0f;
    goingUp = true;
}

void FloatComponent::Update(float dt) {
    auto p = parent.lock();
    glm::vec3 realPos = p->transform.position - glm::vec3(0.0f,currentOffset,0.0f);
    if(goingUp) {
        currentOffset += dt * floatSpeed;
        if (currentOffset > floatRange) {
            currentOffset = floatRange;
            goingUp = false;
        }
    }
    else{
        currentOffset -= dt * floatSpeed;
        if(currentOffset < -floatRange){
            currentOffset = -floatRange;
            goingUp = true;
        }
    }
    p->transform.position = realPos + glm::vec3(0.0f,currentOffset,0.0f);
}

void FloatComponent::OnLeave() {

}

bool FloatComponent::LoadFromFile(const TTH::IniFile& file) {
    file.GetValue("floatRange", floatRange);
    file.GetValue("floatSpeed", floatSpeed);
    return true;
}

FloatComponent::FloatComponent()
: floatRange(1.0f), floatSpeed(1.0f){}


