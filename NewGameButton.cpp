
#include <TTH/SceneManagement/SceneManager.h>
#include "NewGameButton.h"

void NewGameButton::Start() {
    sprite = GetParent()->GetComponent<TTH::SpriteComponent>();
}

void NewGameButton::Update(float dt) {

}

void NewGameButton::OnLeave() {

}

void NewGameButton::OnMouseDown(int button) {
    if(button == 0)
        TTH::SceneManager::LoadNextSceneRequest("TestScene");
}

void NewGameButton::OnMouseEnter() {
    sprite->SetTexture("button_hovered");
}

void NewGameButton::OnMouseLeave() {
    sprite->SetTexture("button");
}