
#include <TTH/Input/Input.h>
#include "QuitButton.h"

void QuitButton::Start() {
    sprite = GetParent()->GetComponent<TTH::SpriteComponent>();
}

void QuitButton::Update(float dt) {

}

void QuitButton::OnLeave() {

}

void QuitButton::OnMouseDown(int button) {
    if(button == 0)
        TTH::Input::QuitGame();
}

void QuitButton::OnMouseEnter() {
    sprite->SetTexture("button_hovered");
}

void QuitButton::OnMouseLeave() {
    sprite->SetTexture("button");
}