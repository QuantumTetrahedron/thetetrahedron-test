
#ifndef THETETRAHEDRONTEST_QUITTOMENUBUTTON_H
#define THETETRAHEDRONTEST_QUITTOMENUBUTTON_H

#include <TTH/Core/BehaviourComponent.h>
#include <TTH/UI/SpriteComponent.h>
#include <TTH/Core/ComponentFactory.h>

class QuitToMenuButton : public TTH::BehaviourComponent {
public:

    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

    void OnMouseDown(int button) override;
    void OnMouseEnter() override;
    void OnMouseLeave() override;
private:

    std::shared_ptr<TTH::SpriteComponent> sprite;

    TTH_COMPONENT(QuitToMenuButton)
};


#endif //THETETRAHEDRONTEST_QUITTOMENUBUTTON_H
