
#ifndef THETETRAHEDRONTEST_MAINMENUMANAGER_H
#define THETETRAHEDRONTEST_MAINMENUMANAGER_H


#include <TTH/Core/BehaviourComponent.h>
#include <TTH/Audio/AudioSource.h>

class MainMenuManager : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;
private:
    float volume;
    std::shared_ptr<TTH::AudioSource> audio;

    TTH_COMPONENT(MainMenuManager)
};


#endif //THETETRAHEDRONTEST_MAINMENUMANAGER_H
