
#ifndef THETETRAHEDRONTEST_CLICKABLECOMPONENT_H
#define THETETRAHEDRONTEST_CLICKABLECOMPONENT_H


#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"
#include "TTH/Graphics/LightComponent.h"
#include "TTH/Graphics/RenderComponent.h"
#include "PointLight.h"

class ClickableComponent : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;

    void OnMouseDown(int button) override;
    void OnMouseEnter() override;
    void OnMouseLeave() override;

private:
    std::shared_ptr<PointLight> lc;
    std::shared_ptr<TTH::RenderComponent> rc;

    TTH_COMPONENT(ClickableComponent)
};


#endif //THETETRAHEDRONTEST_CLICKABLECOMPONENT_H
