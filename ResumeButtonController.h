
#ifndef THETETRAHEDRONTEST_RESUMEBUTTONCONTROLLER_H
#define THETETRAHEDRONTEST_RESUMEBUTTONCONTROLLER_H

#include "TTH/Core/Engine.h"
#include "TTH/UI/UI.h"

class ResumeButtonController : public TTH::BehaviourComponent{
public:
    void Start() override;

    void Update(float dt) override;

    void OnLeave() override;

    void OnMouseDown(int button) override;
    void OnMouseEnter() override;
    void OnMouseLeave() override;

protected:
    std::shared_ptr<TTH::SpriteComponent> sprite;

    TTH_COMPONENT(ResumeButtonController)
};


#endif //THETETRAHEDRONTEST_RESUMEBUTTONCONTROLLER_H
