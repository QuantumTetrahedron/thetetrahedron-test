
#ifndef THETETRAHEDRONTEST_TESTSCENEMANAGER_H
#define THETETRAHEDRONTEST_TESTSCENEMANAGER_H


#include "TTH/Core/BehaviourComponent.h"
#include "TTH/Core/ComponentFactory.h"

class TestSceneManager : public TTH::BehaviourComponent{
public:
    void Start() override;
    void Update(float dt) override;
    void OnLeave() override;
private:
    TTH_COMPONENT(TestSceneManager)
};


#endif //THETETRAHEDRONTEST_TESTSCENEMANAGER_H
