
#include <TTH/Core/ObjectManager.h>
#include "Arrow.h"

void Arrow::Start() {
    modelSelector = TTH::ObjectManager::GetObject(objName)->GetComponent<ModelSelector>();
    rc = GetParent()->GetComponent<TTH::RenderComponent>();
}

void Arrow::Update(float dt) {

}

void Arrow::OnLeave() {

}

bool Arrow::LoadFromFile(const TTH::IniFile &file) {
    file.RequireValue("direction", direction);
    file.RequireValue("modelSelector", objName);
    return true;
}

void Arrow::OnMouseEnter() {
    rc->colorTint = glm::vec3(1.0f, 0.0f, 0.0f);
}

void Arrow::OnMouseLeave() {
    rc->colorTint = glm::vec3(1.0f);
}

void Arrow::OnMouseDown(int button) {
    if(button == 0) {
        modelSelector->ChangeModel(direction);
    }
}
